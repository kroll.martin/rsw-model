from RSW_database import RSWDatabase
import numpy as np
import pandas as pd
import os

call_sign = 'trigonometric_vs_kriging'
assert not os.path.exists('/' + call_sign); 'there was already a simulation study with same call sign'
os.mkdir(call_sign)

F_string_grid = ['F' + str(x) for x in [1,2,3,4,5,6]]
N_grid = [9,17,33,65]
mode_grid = ['simple']
nu_grid = [0.5,1.5,2.5]

### kriging with fixed nu ###

results_kriging = []

for F_string in F_string_grid:
	RSW = RSWDatabase(F_string = F_string)
	# test data
	x_test = np.linspace(0,RSW.theta_max,200)
	y_true = RSW.simulate_energies(x = x_test)
	for N_obs in N_grid:
		RSW.create_startdesign(N = N_obs)
		RSW.simulate_energies()
		for nu in nu_grid:
			nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(mode='simple',nu_min=nu,nu_max=nu, scale_min = RSW.theta_max, scale_max = RSW.theta_max)
			
			y_Kriging, CN = RSW.kriging_Matern(x_test,nu=nu_hat,scale=scale_hat,sigma=sigma_hat)
			
			mse = np.mean(np.square(y_true - y_Kriging))
			sup = np.amax(np.abs(y_true - y_Kriging))
			
			results_kriging.append([F_string,'simple',N_obs,round(nu_hat,4),round(scale_hat,4),round(sigma_hat,4),round(mse,4),round(sup,4),round(CN,4)])

results_df_kriging = pd.DataFrame(data=np.array(results_kriging),index=None,columns=['F_string','mode','N_obs','nu','scale','sigma','mse','sup','CN'])
results_df_kriging.to_csv(call_sign + '/results_kriging.csv',sep=',',index=False)

### kriging nu via MLE

results_kriging_MLE = []

for F_string in F_string_grid:
	RSW = RSWDatabase(F_string = F_string)
	# test data
	x_test = np.linspace(0,RSW.theta_max,200)
	y_true = RSW.simulate_energies(x = x_test)
	for N_obs in N_grid:
		print(F_string,N_obs)
		RSW = RSWDatabase(F_string = F_string)
		RSW.create_startdesign(N = N_obs)
		RSW.simulate_energies()
		nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(mode='simple',nu_min=0.5,nu_max=2.5, scale_min = RSW.theta_max, scale_max = RSW.theta_max)
		
		y_Kriging, CN = RSW.kriging_Matern(x_test,nu=nu_hat,scale=scale_hat,sigma=sigma_hat)
		
		mse = np.mean(np.square(y_true - y_Kriging))
		sup = np.amax(np.abs(y_true - y_Kriging))
		
		results_kriging_MLE.append([F_string,'simple',N_obs,nu_hat,scale_hat,sigma_hat,mse,sup,CN])

results_df_kriging_MLE = pd.DataFrame(data=np.array(results_kriging_MLE),index=None,columns=['F_string','mode','N_obs','nu','scale','sigma','mse','sup','CN'])
results_df_kriging_MLE.to_csv(call_sign + '/results_kriging_MLE.csv',sep=',',index=False)

### trigonometric

results_trig = []

for F_string in F_string_grid:
	RSW = RSWDatabase(F_string = F_string)
	# test data
	x_test = np.linspace(0,RSW.theta_max,200)
	y_true = RSW.simulate_energies(x = x_test)
	for N_obs in N_grid:
		RSW.create_startdesign(N = N_obs)
		RSW.simulate_energies()
		
		y_trig = RSW.trig_interpolation(x_test)
		
		mse = np.mean(np.square(y_true - y_trig))
		sup = np.amax(np.abs(y_true - y_trig))
		
		results_trig.append([F_string,N_obs,mse,sup])

results_df_trig = pd.DataFrame(data=np.array(results_trig),index=None,columns=['F_string','N_obs','mse','sup'])
results_df_trig.to_csv(call_sign + '/results_trig.csv',sep=',',index=False)
