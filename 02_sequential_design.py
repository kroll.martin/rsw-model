from RSW_database import RSWDatabase
import numpy as np
import pandas as pd
import os

call_sign = 'sequential_design'
assert not os.path.exists('/' + call_sign); 'there was already a simulation study with same call sign'
os.mkdir(call_sign)
os.mkdir(call_sign + '/designs')

F_string_grid = ['F' + str(x) for x in [1,2,3,4,5,6]]
N_init_grid = [9,17,33,65]
nu_grid = [0.5,1.5,2.5]
N_max = 65
N_cand = 75

# write simulation setup into a file

f = open(call_sign + '/setup.txt', 'w')
f.write('#F_string_grid\n')
f.write(' '.join(F_string_grid) + '\n')
f.write('#nu_grid\n')
f.write(' '.join([str(x) for x in nu_grid]) + '\n')
f.write('#N_init_grid\n')
f.write(' '.join([str(x) for x in N_init_grid]) + '\n')
f.write('#N_max\n')
f.write(str(N_max) + '\n')
f.write('#N_cand\n')
f.write(str(N_cand) + '\n')
f.close()

for F_string in F_string_grid:
	RSW = RSWDatabase(F_string = F_string)
	for nu in nu_grid:
		for N_init in N_init_grid:
			# create initial design
			RSW.create_startdesign(N=N_init)
			# simulate energies on the initial design
			RSW.simulate_energies()
			nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(nu_min=nu,nu_max=nu,scale_min=RSW.theta_max,scale_max=RSW.theta_max)
			while len(RSW.x) < N_max:
				RSW.find_next_point_jackknife_Matern(nu=nu_hat,scale=scale_hat,sigma=sigma_hat,N_cand=N_cand,N_grid = 501)
				RSW.simulate_energies()
				nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(nu_min=nu,nu_max=nu,scale_min=RSW.theta_max,scale_max=RSW.theta_max)
			data = np.transpose(np.vstack((RSW.x,RSW.y)))
			np.savetxt(call_sign + '/designs/' + F_string + '_N_init_' + str(N_init) + '_N_max_' + str(N_max) + '_nu_' + str(nu).replace('.','_') + '.csv', data, delimiter=',')
