import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

call_sign = 'sequential_design'

df = pd.read_csv(call_sign + '/results.csv')

for F_string in ['F' + str(x) for x in [2]]:
	for criterion in ['sup']: # 
		for nu in [2.5]:
			aux_df = df[(df['F_string'] == F_string) & (df['mode'] == 'simple') & (df['nu'] == nu)]
			
			N_total_grid = [9,17,33,65]
			N_init_grid = [9,17,33,65]
			
			def f(N_total,N_init,criterion):
				if N_total < N_init:
					value = aux_df[(aux_df['N_total'] == N_init) & (aux_df['N_init'] == N_total)][criterion]
					return(float(value))
				else:
					value = aux_df[(aux_df['N_total'] == N_total) & (aux_df['N_init'] == N_init)][criterion]
					return(float(value))
			
			im = [[f(N_total,N_init,criterion=criterion) for N_init in N_init_grid] for N_total in np.flip(N_total_grid)]
			
			data_plot = pd.DataFrame(im, columns=N_init_grid, index=np.flip(N_total_grid))
			
			# Generate a mask for the upper triangle
			mask = np.array([[(N_total < N_init) for N_init in N_init_grid] for N_total in np.flip(N_total_grid)], dtype='bool')		
						
			# Set up the matplotlib figure
			f, ax = plt.subplots()
			ax.set_xlabel('angle')
			ax.set_ylabel('energy')
			
			# Generate a custom diverging colormap
			cmap = sns.diverging_palette(230, 20, as_cmap=True)
			
			# Draw the heatmap with the mask and correct aspect ratio
			sns.heatmap(data_plot, mask=mask, cmap=cmap,linewidths=.5,annot=True)
			plt.xlabel('$N_{\mathrm{init}}$',fontsize=12) 
			plt.ylabel('$N_{\mathrm{total}}$',fontsize=12)
			f.tight_layout()
			plot_path = call_sign + '/plots/' + F_string + '_' + str(nu).replace('.','_') + '_' + criterion + '.pdf'
			plt.savefig(plot_path,dpi=600)
			plt.close()
			

