from RSW_database import RSWDatabase
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os

os.mkdir('example')

# create RSW database from string ('Fx' for x = 1,...,6 are allowed)
RSW = RSWDatabase(F_string = 'F2')

# create start design
N_total = 17
RSW.create_startdesign(N=N_total)

# simulate the energies on the start design
RSW.simulate_energies()

# define (fine) test data of angles where we would like to predict the GB energy
x_test = np.linspace(0,RSW.theta_max,181)

# compute true energy values on the test data grid
y_true = RSW.simulate_energies(x = x_test)

# we take nu = 2.5, put l = thetamax and estimate sigma via MLE
nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(mode='simple',nu_min=2.5,nu_max=2.5, scale_min = RSW.theta_max, scale_max = RSW.theta_max)
# calculate Kriging predictions
y_Kriging, _ = RSW.kriging_Matern(x_test,nu=nu_hat,scale=scale_hat,sigma=sigma_hat)

# plot the results
fig, ax1 = plt.subplots()
ax1.set_xlabel(r'misorientation angle [$^\circ$]',fontsize=16)
ax1.set_ylabel('GB-energy [' + r'$J/m^2$' + ']',fontsize=16)
ax1.plot(x_test,y_Kriging,color='b',linewidth=0.95)
ax1.plot(x_test,y_true,ls='--',color='k',linewidth=0.95)
ax1.scatter(RSW.x, RSW.y, color='k', marker="^", s=25)
fig.tight_layout()
plt.savefig('example/simple_kriging_F2_n_obs_17_nu_2_5.pdf', dpi=600)
plt.close()

# calculate trigonometric predictions
y_trigonometric = RSW.trig_interpolation(x_test)

# plot the results
fig, ax1 = plt.subplots()
ax1.set_xlabel(r'misorientation angle [$^\circ$]',fontsize=16)
ax1.set_ylabel('GB-energy [' + r'$J/m^2$' + ']',fontsize=16)
ax1.plot(x_test,y_trigonometric,color='b',linewidth=0.75)
ax1.plot(x_test,y_true,ls='--',color='k',linewidth=0.75)
ax1.scatter(RSW.x, RSW.y, color='k', marker="^", s=15)
fig.tight_layout()
plt.savefig('example/trig_interpolation_F2_n_obs_17.pdf', dpi=600)
plt.close()
