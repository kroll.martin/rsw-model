from RSW_database import RSWDatabase
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib as mpl

call_sign = 'sequential_design'
if not os.path.isdir(call_sign + 'plots'):
	os.mkdir(call_sign + '/plots')

F_string_grid = ['F' + str(x) for x in [1,2,3,4,5,6]]
N_init_grid = [9,17,33,65]
nu_grid = [0.5,1.5,2.5]
N_max = 65

results = []

for F_string in F_string_grid:
	RSW = RSWDatabase(F_string = F_string)
	x_test = np.linspace(0.01,RSW.theta_max-0.01,200)
	y_true = RSW.simulate_energies(x = x_test)
	for nu in nu_grid:
		for N_init in N_init_grid:
			data = np.genfromtxt(call_sign + '/designs/' + F_string + '_N_init_' + str(N_init) + '_N_max_' + str(N_max) + '_nu_' + str(nu).replace('.','_') + '.csv',delimiter=',')
			for N_total in N_init_grid:
				if N_total < N_init:
					continue
				RSW.x = data[0:N_total,0]
				RSW.y = data[0:N_total,1]
				nu_hat, scale_hat, sigma_hat = RSW.mle_Matern(mode='simple',nu_min=nu,nu_max=nu, scale_min = RSW.theta_max, scale_max = RSW.theta_max)
				y_kriging, CN = RSW.kriging_Matern(x_test,nu=nu_hat,scale=scale_hat,sigma=sigma_hat)
				mse = np.mean(np.square(y_true - y_kriging))
				sup = np.amax(np.abs(y_true - y_kriging))
				results.append([F_string,'simple',N_init,N_total,nu_hat,scale_hat,sigma_hat,mse,sup,CN])
				# plot of interpolator, true function, and error
				plot_path = call_sign + '/plots/' + F_string + '_nu_' + str(nu).replace('.','_') + '_N_init_' + str(N_init) + '_N_total_' + str(N_total) + '.png'
				fig, ax1 = plt.subplots()
				ax1.set_xlabel('misorientation angle [$^\circ$]')
				ax1.set_ylabel('GB-energy [' + r'$J/m^2$' + ']')
				ax1.plot(x_test,y_kriging,color='b',linewidth=0.75)
				ax1.plot(x_test,y_true,ls='--',color='k',linewidth=0.75)
				ax1.scatter(RSW.x, np.zeros(len(RSW.x)), color='k', marker="^", s=2)
				# second part of the plot (absolute error in red and :, right axis)
				ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
				color = 'tab:red'
				ax2.set_ylabel('absolute error [' + r'$J/m^2$' + ']', color=color)  # we already handled the x-label with ax1
				ax2.plot(x_test,np.absolute(y_kriging-y_true),color=color,ls=':',linewidth=0.8)
				ax2.tick_params(axis='y', labelcolor=color)
				fig.tight_layout()
				plt.savefig(plot_path,dpi=600)
				plt.close()
				# plot of dynamics
				if N_total > N_init:
					plot_path = call_sign + '/plots/' + F_string + '_nu_' + str(nu_hat).replace('.','_') + '_N_init_' + str(N_init) + '_N_total_' + str(len(RSW.x)) + '_dynamics.pdf'
					fig, ax1 = plt.subplots()
					ax1.set_xlabel('misorientation angle [$^\circ$]', fontsize=12)
					ax1.set_ylabel('sequential design stage', fontsize=12)
					ax1.scatter(RSW.x[0:N_init],np.zeros(N_init),color='k',marker = "^",s=2)
					ax1.plot(RSW.x[N_init:],np.arange(1,len(RSW.x)-N_init + 1),color='0.8',linewidth=0.8)
					ax1.scatter(RSW.x[N_init:],np.arange(1,len(RSW.x)-N_init + 1),color='k',marker="^",s=2)
					ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
					color = 'tab:gray'
					ax2.set_ylabel('GB-energy [' + r'$J/m^2$' + ']', color=color, fontsize=12)  # we already handled the x-label with ax1
					ax2.plot(x_test,y_true,color=color,linewidth=0.8,alpha=0.8)
					ax2.tick_params(axis='y', labelcolor=color)
					fig.tight_layout()
					plt.savefig(plot_path,dpi=600)
					plt.close()
		
results_df_kriging = pd.DataFrame(data=np.array(results),index=None,columns=['F_string','mode','N_init','N_total','nu','scale','sigma','mse','sup','CN'])
results_df_kriging.to_csv(call_sign + '/results.csv',sep=',',index=False)
		
		
