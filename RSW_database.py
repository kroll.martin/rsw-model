import numpy as np
from scipy.special import gamma, factorial, kv
from scipy.spatial import distance_matrix
import matplotlib.pyplot as plt
import scipy.optimize
import sys

### auxiliary functions

'''
the two following functions are used to define the benchmark energy function in the RSW model

notation is adopted from:

Dette,  H.,  Goesmann,  J.,  Greiff,  C.  &  Janisch,  R.   Efficient  sampling  inmaterials simulation - Exploring the parameter space of grain boundaries. Acta Mater.125, 145–155 (2017)
'''

def f_RSW(x, a = 0.5):
	# auxiliary function to define the segments of the
	if x == 0:
		return(0)
	return(np.sin(np.pi*x/2)*(1 - a * np.log(np.sin(np.pi * x/2))))
	
def F_RSW(x, theta, gamma, k=0):
	res = 0
	for i in range(len(theta) - 1):
	 	if (x > theta[i] and x <= theta[i+1]):
	 		if (gamma[i+1] - gamma[i] < 0):
	 			aux = gamma[i+1] + (gamma[i] - gamma[i+1]) * f_RSW((theta[i+1] - x)/(theta[i+1] - theta[i]))
	 		else:
	 			aux = gamma[i] + (gamma[i+1] - gamma[i]) * f_RSW((x-theta[i])/(theta[i+1] - theta [i]))
	 		res = res + aux
	return(res)

# definition of Matern covariance function in 1d
def k_Matern_1d(xx,yy,nu,scale,sigma):
	'''
	xx, yy: array of points
	
	output: matrix containing the covariance between the points in xx (indexed by the rows) and the points in yy (index by the columns)
	'''
	xx_reshaped = xx.reshape((len(xx),1))
	yy_reshaped = yy.reshape((len(yy),1))
	dist_mat=distance_matrix(xx_reshaped,yy_reshaped,p=2)
	with np.errstate(invalid='ignore'):
		return(np.where(dist_mat == 0, sigma**2, sigma**2 * 2**(1-nu)/gamma(nu) * np.power(np.sqrt(2*nu)*dist_mat/scale,nu) * kv(nu, np.sqrt(2*nu)*dist_mat/scale)))

# definition of the Kriging interpolator in 1d
def kriging_Matern_1d(x_obs,y_obs,x_kriging,nu,scale,sigma,mode,nugget=0):
	'''
	x_obs: array of observations/training data
	y_obs: array of 'energies' observed at the locations from x_obs
	x_kriging: array of locations where we want to predict the energy
	nu, scale, sigma: hyperparameters of the covariance function
	nugget: parameter for regularization and to ensure numerical stability (a priori set to 0 means no regularization)
	'''
	aux_pred = x_kriging.reshape((len(x_kriging),1))
	aux_obs = x_obs.reshape((len(x_obs),1))
	G = k_Matern_1d(aux_obs,aux_obs,nu=nu,scale=scale,sigma=sigma) + nugget * np.eye(len(aux_obs))
	if mode == 'ordinary':
		F = np.array([[1] for _ in aux_obs])
		M,_,_,_ = np.linalg.lstsq(G,F,rcond=None)
		A = np.matmul(np.transpose(F),M)
		M,_,_,_ = np.linalg.lstsq(G,y_obs,rcond=None)
		v = np.matmul(np.transpose(F),M)
		beta_hat,_,_,_ = np.linalg.lstsq(A,v,rcond=None)	
		residuals = y_obs - np.matmul(F, beta_hat)
		G_inv_res,_,_,_ = np.linalg.lstsq(G,residuals,rcond=None)
		k = k_Matern_1d(aux_pred,aux_obs,nu=nu,scale=scale,sigma=sigma)
		y_kriging = beta_hat + np.matmul(k,G_inv_res)
	elif mode == 'simple':
		M,_,_,_ = np.linalg.lstsq(G,y_obs,rcond=None)
		y_kriging = np.matmul(k_Matern_1d(aux_pred,aux_obs,nu=nu,scale=scale,sigma=sigma), M)
	return(y_kriging)

def create_trig_basis(n,T):
	'''
	function returns return list of the first n trigonometric basis function on the interval [0,T]
	n : number of desired basis functions
	T : upper bound of the interval (lower bound is assumed to be 0)
	output: list of trigonometric functions (data type: list of functions)
	'''
	L = np.floor((n-1)/2)
	L = L.astype(int)
	# function outputs list of 2*L+1
	# initialize list of basis functions
	trig_basis = []
	# later: if elif over symmetry groups
	# loop over all considered levels l of spherical harmonics
	for l in range(L + 1):
		if l == 0:
			aux_trig = [lambda x, l=l: 1/np.sqrt(T)]
		elif l > 0:
			aux_trig = [lambda x, l=l: np.sqrt(2/T) * np.cos(2*np.pi*x*l/T), lambda x, l=l: np.sqrt(2/T) * np.sin(2*np.pi *x*l/T)]
		trig_basis.append(aux_trig)
	if n % 2 == 0:
		aux_trig = [lambda x, l=L+1: np.sqrt(2/T) * np.cos(2*np.pi*x*l/T)]
		trig_basis.append(aux_trig)
	trig_basis = [item for sublist in trig_basis for item in sublist]
	return(trig_basis)

# class definition

class RSWDatabase():
	'''
	model for RSWDatabase
	'''
	def __init__(self, **kwargs):
		'''
		constructor
		'''
		# check kwargs
		if 'F_string' in kwargs:
			if kwargs['F_string'] == 'F1':
				self.theta_max = 90
				self.theta_vec = [0, 27.616, 36, 44.963, 52, 60, 90]
				self.gamma_vec = [0, 0.893, 0.8315, 0.933, 0.896, 0.775, 0]
			elif kwargs['F_string'] == 'F2':
				self.theta_max = 180
				self.theta_vec = [0, 20.775, 50, 80.4199, 110, 150.49, 180]
				self.gamma_vec = [0, 0.683, 0.285, 0.664, 0.043, 0.826, 0]
			elif kwargs['F_string'] == 'F3':
				self.theta_max = 60
				self.theta_vec = [0, 31.513, 60];
				self.gamma_vec = [0, 0.837, 0.619]
			elif kwargs['F_string'] == 'F4':
				self.theta_max = 45
				self.theta_vec = [0, 45]
				self.gamma_vec = [0, 0.684]
			elif kwargs['F_string'] == 'F5':
				self.theta_max = 90
				self.theta_vec = [0, 30.309, 70, 90]
				self.gamma_vec = [0, 0.909, 0.664, 0.597]
			elif kwargs['F_string'] == 'F6':
				self.theta_max = 60
				self.theta_vec = [0, 28.361, 60]
				self.gamma_vec = [0, 0.347 , 0.043]
	
	def create_startdesign(self,N):
		self.x = np.linspace(0,self.theta_max,N)
		return(self.x)
		
	def simulate_energies(self,**kwargs):
		if 'x' in kwargs:
			x = kwargs['x']
			energies = [F_RSW(angle,theta=self.theta_vec,gamma=self.gamma_vec) for angle in x]
			return(energies)
		else:
			self.y = [F_RSW(x,theta=self.theta_vec,gamma=self.gamma_vec) for x in self.x]
			return(self.y)
		
	### statistical routines trigonometric interpolation ###
	def trig_interpolation(self,x_series,**kwargs):
		'''
		computes trigonometric interpolator based on training data in RSW.x and RSW.y
		x_series: array containing locations where one wants to perform predictions
		'''
		n_obs = len(self.x)
		if 'L' in kwargs:
			L = kwargs['L']
			trig_basis = create_trig_basis(L,self.theta_max)
		else:
			trig_basis = create_trig_basis(n_obs,self.theta_max)
		# least squares method
		if 'w' in kwargs:
			# version with weights
			w = kwargs['w']
			assert len(w) == n_obs; 'error'
			W_sqrt = np.diag(np.sqrt(w))
			X = np.array([[f(x) for f in trig_basis] for x in self.x])
			X = np.matmul(W_sqrt,X)
			Y = np.array(self.y).reshape((len(self.y),1))
			Y = np.matmul(W_sqrt,Y)
			# use the lstsq routine from numpy
			beta_hat,_,_,s_values = np.linalg.lstsq(X,Y,rcond=None)
			beta_hat = beta_hat.flatten()
		else:
			x_aux = np.sort(self.x)
			ind_sort = np.argsort(x_aux)
			x_aux = x_aux[ind_sort]
			y_aux = np.array(self.y).flatten()
			y_aux = y_aux[ind_sort]
			x_min = x_aux[0]
			y_aux = np.array(y_aux).reshape((len(self.y),1))
			y_0 = y_aux[0]
			x_max = x_aux[-1]
			assert x_min != x_max
			y_end = y_aux[-1]
			a = (y_0 - y_end)/(x_min - x_max)
			b = y_0 - a * x_min
			a = a[0]
			b = b[0]
			y_trans = y_aux - (a * x_aux.reshape((len(self.x),1)) + b)
			y_trans = np.array(y_trans).reshape((len(self.y),1))
			assert y_trans.flatten()[0] == 0
			assert y_trans.flatten()[-1] == 0
			X = np.array([[f(x) for f in trig_basis] for x in x_aux])
			#Y = np.array(self.y).reshape((len(self.y),1))
			# use the lstsq routine from numpy
			beta_hat,_,_,s_values = np.linalg.lstsq(X,y_trans,rcond=None)
			beta_hat = beta_hat.flatten()
		
		# construct the estimator as a python function of one argument
		def ons_estimator(x):
			value = a*x + b + np.dot(beta_hat,np.array([f(x) for f in trig_basis]))
			#print(value)
			return(value)
		
		# compute the interpolator at the desired locations
		y_series = np.array([ons_estimator(x) for x in x_series])
		return(y_series)

	### statistical routines Matern ###	
	
	def mle_Matern(self,mode='simple',nu_min=0.5,nu_max=1.5,scale_min=0.01,scale_max=1000,nugget=0,N_iter_opt=10):		
		# start mle
		assert len(self.x) == len(self.y)
		n_obs = len(self.x)
		if (nu_min != nu_max) or (scale_min != scale_max):
			def minus_MLE_profile(params): # auxiliary function for mle
				nu, scale = params
				G = k_Matern_1d(self.x,self.x,nu=nu,scale=scale,sigma=1.0)
				if mode == 'ordinary':
					F = np.array([[1] for _ in self.x])
					M,_,_,_ = np.linalg.lstsq(G,F,rcond=None)
					A = np.matmul(np.transpose(F),M)
					M,_,_,_ = np.linalg.lstsq(G,self.y,rcond=None)
					v = np.matmul(np.transpose(F),M)
					beta_hat,_,_,_ = np.linalg.lstsq(A,v,rcond=None)
					M,_,_,_ = np.linalg.lstsq(G,self.y-np.matmul(F,beta_hat),rcond=None)
					sigma_square_hat = np.matmul(np.transpose(self.y-np.matmul(F,beta_hat)),M)/n_obs
				elif mode == 'simple':
					M,_,_,_ = np.linalg.lstsq(G,self.y,rcond=None)
					sigma_square_hat = np.matmul(np.transpose(self.y),M)/n_obs
				# end of if... elif...
				_, log_det_G = np.linalg.slogdet(G)
				res = -0.5 * log_det_G - 0.5 * n_obs * np.log(sigma_square_hat) - 0.5 * n_obs * np.log(2*np.pi)
				return(-res)
			
			initial_guess = [np.random.uniform(nu_min,nu_max,1),np.random.uniform(scale_min,scale_max,1)]
			result = scipy.optimize.minimize(minus_MLE_profile,initial_guess,bounds=((nu_min,nu_max),(scale_min,scale_max)))
			nu_hat, scale_hat = result.x
		else:
			# else part captures the case where lower and upper bound coincide for all hyperparameters
			# necessary because scipy.optimize.minimize cannot deal with this case (yields result.success = False)
			assert (nu_min == nu_max) and (scale_min == scale_max)
			nu_hat = nu_min
			scale_hat = scale_min
				
		G = k_Matern_1d(self.x,self.x,nu=nu_hat,scale=scale_hat,sigma=1.0)	
		if mode == 'ordinary':
			F = np.array([[1] for _ in self.x])
			M,_,_,_ = np.linalg.lstsq(G,F,rcond=None)
			A = np.matmul(np.transpose(F),M)
			M,_,_,_ = np.linalg.lstsq(G,self.y,rcond=None)
			v = np.matmul(np.transpose(F),M)
			beta_hat,_,_,_ = np.linalg.lstsq(A,v,rcond=None)
			M,_,_,_ = np.linalg.lstsq(G,self.y-np.matmul(F,beta_hat),rcond=None)
			sigma_square_hat = np.matmul(np.transpose(self.y-np.matmul(F,beta_hat)),M)/n_obs
			sigma_hat = np.sqrt(sigma_square_hat)
		elif mode == 'simple':
			M,_,_,_ = np.linalg.lstsq(G,self.y,rcond=None)
			sigma_square_hat = np.matmul(np.transpose(self.y),M)/n_obs
			sigma_hat = np.sqrt(sigma_square_hat)
		return((nu_hat,scale_hat,sigma_hat))

	def kriging_Matern(self,x_kriging,nu,scale,sigma,mode='simple'):
		'''
		
		output:
		- y_kriging: interpolated energies
		- CN: conditions number of the covariance matrix in kriging procedure
		'''
		aux_pred = x_kriging.reshape((len(x_kriging),1))
		aux_obs = self.x.reshape((len(self.x),1))
		G = k_Matern_1d(aux_obs,aux_obs,nu=nu,scale=scale,sigma=sigma)
		condition_number = np.linalg.cond(G)
		# distinction between 'simple' and 'ordinary' kriging
		if mode == 'ordinary':
			F = np.array([[1] for _ in aux_obs])
			M = np.linalg.solve(G,F)
			A = np.matmul(np.transpose(F),M)
			M = np.linalg.solve(G,self.y)
			v = np.matmul(np.transpose(F),M)
			beta_hat = np.linalg.solve(A,v)	
			residuals = self.y - np.matmul(F, beta_hat)
			G_inv_res = np.linalg.solve(G,residuals)
			k = k_Matern_1d(aux_pred,aux_obs,nu=nu,scale=scale,sigma=sigma)
			y_kriging = beta_hat + np.matmul(k,G_inv_res)
		elif mode == 'simple':
			M = np.linalg.solve(G,self.y)
			y_kriging = np.matmul(k_Matern_1d(aux_pred,aux_obs,nu=nu,scale=scale,sigma=sigma), M)
		return((y_kriging,condition_number))
		
	def find_next_point_jackknife_Matern(self,scale,nu,sigma,mode='simple',N_cand=10,N_grid = 501):
		assert mode in ['simple','ordinary']; 'assertion error: mode must be simple or ordinary'
		# implements the algorithm from
		# Kleijnen and van Beers: Application-driven sequential designs for simulation experiments: Kriging metamodelling
		# Journal of the Operational Research Society (2004) 55, 876--883
		
		# (1) a fine grid of n_grid points from space filling design is taken
		# (2) from this grid n_cand points are taken according to s^2 (posterior variance) criterion
		# (3) on these points x_cand the actual algorithm from the paper is applied
		# algorithm appends new point to self.x (but does not compute the corresponding energy; for this need to run self.simulate_energies())
		N_obs = len(self.x)
		grid = np.linspace(0,self.theta_max,N_grid) # (1)
		aux = np.copy(self.x)
		x_cand = []
		for _ in np.arange(N_cand): # (2)
			# computation of s^2 criterion
			# to do: distinction between simple and ordinary
			G = k_Matern_1d(aux,aux,nu=nu,scale=scale,sigma=sigma)
			k = k_Matern_1d(grid,aux,nu=nu,scale=scale,sigma=sigma)
			with np.errstate(invalid='ignore',divide='ignore'):
				M,_,_,_ = np.linalg.lstsq(G,np.transpose(k),rcond=None)
			aux1 = np.diag(np.matmul(k,M))
			if mode == 'simple':
				uncertainty_cand = 1 - aux1
			elif mode == 'ordinary':
				F = np.array([[1] for _ in aux])
				aux3,_,_,_ = np.linalg.lstsq(G,F,rcond=None)
				Q = np.matmul(np.transpose(F),aux3)
				f = np.array([[1] for _ in grid])
				aux3,_,_,_ = np.linalg.lstsq(G,np.transpose(k),rcond=None)
				h = np.transpose(f) - np.matmul(np.transpose(F),aux3)
				aux3,_,_,_ = np.linalg.lstsq(Q,h,rcond=None)
				aux2 = np.diag(np.matmul(np.transpose(h),aux3))
				uncertainty_cand = 1 - aux1 + aux2 
			ind_max = np.argmax(uncertainty_cand)
			aux = np.append(aux,grid[ind_max])
			x_cand = np.append(x_cand,grid[ind_max])
		# from now on: (3)
		# determine usual Kriging estimator on all N_obs observations collected up to now
		x_cand_kriging, _ = self.kriging_Matern(x_cand,nu=nu,scale=scale,sigma=sigma,mode=mode)
		
		# kriging for all sample reduced via one point
		x_cand_kriging_array = np.zeros((N_obs,N_cand))
		for j in np.arange(N_obs):
			x_obs_wo_j = np.delete(self.x,j)
			y_obs_wo_j = np.delete(self.y,j)
			aux = kriging_Matern_1d(x_obs_wo_j,y_obs_wo_j,x_cand,nu=nu,scale=scale,sigma=sigma,mode=mode)
			x_cand_kriging_array[j,:] = N_obs * x_cand_kriging - (N_obs - 1) * aux
			
		y_bar = np.mean(x_cand_kriging_array,axis=0)
		for j in np.arange(N_obs):
			x_cand_kriging_array[j,:] = np.square(x_cand_kriging_array[j,:] - y_bar)
		
		# jackknife variance
		s_square = np.sum(x_cand_kriging_array,axis=0)/(N_obs * (N_obs - 1))
		sort_grid_cand_ind = np.argsort(x_cand)
		ind_next_point = np.argmax(s_square)
		next_point = x_cand[ind_next_point]
		self.x = np.append(self.x, next_point)
		return((next_point,np.amax(s_square)))
